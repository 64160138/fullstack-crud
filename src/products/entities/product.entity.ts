import { Column, CreateDateColumn, PrimaryGeneratedColumn } from 'typeorm';

export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 64 })
  name: string;
  @Column({ type: 'float' })
  price: number;
  @CreateDateColumn()
  createdAt: Date;
  @CreateDateColumn()
  updatedAt: Date;
  @CreateDateColumn()
  deletedAt: Date;
}
